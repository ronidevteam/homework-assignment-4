// Menu object with hard coded items


// Hardcoded menu items
const items = [{
        name: 'Hawaiian',
        price: 5
    },
    {
        name: 'Pepperoni',
        price: 7
    },
    {
        name: 'Margherita',
        price: 6.5
    }
];


// Menu object to be exported
const menu = {};

menu.getMenu = () => {
    return items;
};
menu.getItem = (itemName) => {
    return items.find((item) => {
        return itemName.toLowerCase() === item.name.toLowerCase();
    });
};


// Export module
module.exports = menu;