/**
 * Helpers for various tasks
 */

// Dependencies
const crypto = require('crypto');
const config = require('../config/config');
const https = require('https');
const querystring = require('querystring');
const path = require('path');
const fs = require('fs');

// Container for all helpers
const helpers = {};

// Create SHA256 hash
helpers.hash = str => {
    if (typeof str == 'string' && str.length > 0) {
        return crypto
            .createHmac('sha256', config.hashingSecret)
            .update(str)
            .digest('hex');
    } else {
        return false;
    }
};

// Parse a JSON string to an object in all cases, without throwing
helpers.parseJsonToObject = str => {
    try {
        return JSON.parse(str);
    } catch (e) {
        return {};
    }
};

// Create a string of random alphanumerc charecters of a given length
helpers.createRandomString = strLength => {
    strLength = typeof strLength == 'number' && strLength > 0 ? strLength : false;
    if (strLength) {
        // Define all hte possible charecters taht go into a string
        const possibleCharacters = 'abcdefghijklmnopqrstuvwxyz0123456789';

        // Start the final string
        let str = '';
        for (i = 1; i <= strLength; i++) {
            // Get a random charecter from the possibleCharacters string
            const randomCharacter = possibleCharacters.charAt(
                Math.floor(Math.random() * possibleCharacters.length)
            );
            // Apend this character to the final string
            str += randomCharacter;
        }
        return str;
    } else {
        return false;
    }
};

// Check for valid email
helpers.validateEmail = email => {
    if (typeof (email) == 'string' && email.trim().length > 0) {
        const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegexp.test(email);
    } else {
        return false;
    }
};

// Payment with stripe
helpers.proceedPayment = (amount, callback) => {
    if (typeof (amount) == 'number') {
        // Transform into dolars
        const amountInDolars = amount * 100;
        if (amountInDolars > 500) {
            // Build the post string from an object
            const post_data = querystring.stringify({
                source: 'tok_visa',
                amount: amountInDolars,
                currency: 'usd'
            });
            const post_options = {
                host: 'api.stripe.com',
                protocol: 'https:',
                path: '/v1/charges',
                method: 'POST',
                auth: config.stripe.apiKey
            };

            // Set up the request
            const post_req = https.request(post_options, function (res) {
                let body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });

                res.on('end', () => {
                    body = JSON.parse(body);
                    // Check if the payment was successful
                    if (!body.error) {
                        callback(false);
                    } else {
                        callback(`Could not make payment. ${body.error}`);
                    }
                });
            });
            // post the data
            post_req.write(post_data);
            post_req.end();
        } else {
            callback('The amount cannot be less than $5 ');
        }
    } else {
        callback('Amount must be a number');
    }
};

helpers.sendEmail = (email, totalAmount, callback) => {
    const emailMessage = `
        Your order was successfully proceeded.
        -----------------------------------------
        Total payed: ${totalAmount} $
    `;

    const requestBody = querystring.stringify({
        from: 'Pizza Place pizza@pizza.com',
        to: email,
        subject: 'Order Confirmation',
        text: emailMessage,
    });

    const options = {
        method: 'POST',
        protocol: 'https:',
        host: 'api.mailgun.net',
        path: `/v3/${config.mailgun.accountId}.mailgun.org/messages`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${Buffer.from('api:' + config.mailgun.apiKey).toString('base64')}`,
        },
        payload: requestBody
    };

    // Set up the request
    var post_req = https.request(options, function (res) {
        let body = '';
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', () => {
            body = JSON.parse(body);
            // Check if email was sent successfully
            if (body.id) {
                callback(false);
            } else {
                callback(`Could not send email. ${body.message}`);
            }
        });
    });
    // post the data
    post_req.write(requestBody);
    post_req.end();
};

// Get the string content of a template
helpers.getTemplate = (templateName, data, callback) => {
    templateName = typeof (templateName) == 'string' && templateName.length > 0 ? templateName : false;
    data = typeof (data) == 'object' && data !== null ? data : {};
    if (templateName) {
        const templatesDir = path.join(__dirname, '/../templates/');
        fs.readFile(templatesDir + templateName + '.html', 'utf8', (err, str) => {
            if (!err && str && str.length > 0) {
                // Do interpolation on the string
                const finalString = helpers.interpolate(str, data);
                callback(false, finalString);
            } else {
                callback('No template could be found');
            }
        });
    } else {
        callback('A valid template name was not specified');
    }
};


// Take a given string and a data boject and find/replace all the keys in it
helpers.interpolate = (str, data) => {
    str = typeof (str) == 'string' && str.length > 0 ? str : '';
    data = typeof (data) == 'object' && data !== null ? data : {};

    // Add the templateGlobals do the data object, prepending their key name with 'global'
    for (let keyName in config.templateGlobals) {
        if (config.templateGlobals.hasOwnProperty(keyName)) {
            data['global.' + keyName] = config.templateGlobals[keyName];
        }
    }

    // For each key in the data object, insert its value into the string at the corresponding placehoder
    for (let key in data) {
        if (data.hasOwnProperty(key) && typeof (data[key]) == 'string') {
            let replace = data[key];
            let find = '{' + key + '}';
            str = str.replace(find, replace);
        }
    }
    return str;
};

// Add the universal header and footer to a string and  pass proviced data object to the header and footer
helpers.addUniversalTemplates = (str, data, callback) => {
    str = typeof (str) == 'string' && str.length > 0 ? str : '';
    data = typeof (data) == 'object' && data !== null ? data : {};

    // Get the header
    helpers.getTemplate('_header', data, (err, headerString) => {
        if (!err, headerString) {
            // Get the footer
            helpers.getTemplate('_footer', data, (err, footerString) => {
                if (!err && footerString) {
                    // Add them all together
                    const fullString = headerString + str + footerString;
                    callback(false, fullString);
                } else {
                    callback('Could not find the footer template')
                }
            });
        } else {
            callback('Could not find the header template');
        }
    });
};

// Get the contents of a static (public) asset
helpers.getStaticAsset = (fileName, callback) => {
    fileName = typeof (fileName) == 'string' && fileName.length > 0 ? fileName : false;
    if (fileName) {
        const publicDir = path.join(__dirname, '/../public/');
        fs.readFile(publicDir + fileName, (err, data) => {
            if (!err && data) {
                callback(false, data);
            } else {
                callback('No file could be found')
            }
        });
    } else {
        callback('A valid file name was not specified')
    }
};


// Export the module
module.exports = helpers;