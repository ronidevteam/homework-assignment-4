/**
 * Helper class specific for CLI-related functionalities
 */

// CLI Helpers Class
const cliHelpers = {};



// Create a vertical space
cliHelpers.verticalSpace = (lines) => {
    lines = typeof (lines) == 'number' && lines > 0 ? lines : 1;
    for (let i = 0; i < lines; i++) {
        console.log('');
    }
};

// Create a horizontal line across the screen
cliHelpers.horizontalLine = () => {
    // Get the available screen size
    const width = process.stdout.columns;
    let line = '';
    for (let i = 0; i < width; i++) {
        line += '-';
    }
    console.log(line);
};

// Create centered text on the screen
cliHelpers.centered = (str) => {
    str = typeof (str) == 'string' && str.trim().length > 0 ? str.trim() : '';

    // Get the available screen size
    const width = process.stdout.columns;

    // Calculate the left padding there should be
    const letftPadding = Math.floor((width - str.length) / 2);

    // Put in left padded space before the string itself
    let line = '';
    for (let i = 0; i < letftPadding; i++) {
        line += ' ';
    }
    line += str;
    console.log(line);
};

// Export the module
module.exports = cliHelpers;