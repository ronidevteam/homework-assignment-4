/**
 * CLI-related code
 */

// Dependencies
const readline = require('readline');
const events = require('events');
const _cliHelpers = require('./cli-helpers');
const _menu = require('./menu');
const _data = require('./data');

class _events extends events {}
const e = new _events();

const cli = {};

/**
 * INPUT HANDLERS
 */
e.on('man', (str) => {
    cli.responders.help();
});

e.on('help', (str) => {
    cli.responders.help();
});

e.on('exit', (str) => {
    process.exit(0);
});

e.on('list menu items', (str) => {
    cli.responders.listMenuItems();
});

e.on('list orders', (str) => {
    cli.responders.listOrders(str);
});

e.on('more order info', (str) => {
    cli.responders.moreOrderInfo(str);
});

e.on('list users', (str) => {
    cli.responders.listUsers(str);
});

e.on('more user info', (str) => {
    cli.responders.moreUserInfo(str);
});

/**
 * RESPONDERS
 */

// Responders object
cli.responders = {};

// Help / Man
cli.responders.help = () => {
    const commands = {
        'exit': 'Kill the CLI (and the rest of the application)',
        'man': 'Show this help page',
        'help': 'Alias of the "man" command',
        'list menu items': 'Show a list of all menu items',
        'list orders --recent': 'Show a list of all the orders placed in the system. If "-recent" flag is specified, show only orders made in the last 24 hours"',
        'more order info --{orderId}': 'Show details of a specific order',
        'list users --recent': 'Show a list of all the registered users in the system. If "-recent" flag is specified, show only users registered in the last 24 hours',
        'more user info --{userEmail}': 'Show details of a specific user',
    };

    // Show a header for the help page that is as wide as the screen
    _cliHelpers.horizontalLine();
    _cliHelpers.centered('CLI MANUAL');
    _cliHelpers.horizontalLine();
    _cliHelpers.verticalSpace(2);

    // Show each command, followed by its explanation, in white and yellow respectevelly
    for (let key in commands) {
        if (commands.hasOwnProperty(key)) {
            const value = commands[key];
            let line = '\x1b[33m' + key + '\x1b[0m';
            const padding = 60 - line.length;
            for (let i = 0; i < padding; i++) {
                line += ' ';
            }
            line += value;
            console.log(line);
            _cliHelpers.verticalSpace();
        }
    }
    _cliHelpers.verticalSpace();

    // End with another horizontal line
    _cliHelpers.horizontalLine();
};

// List menu items
cli.responders.listMenuItems = () => {
    const menuItems = _menu.getMenu();
    menuItems.forEach((item) => {
        _cliHelpers.verticalSpace();
        console.log(item.name + ': $' + item.price);
    });
};

// List orders
cli.responders.listOrders = (str) => {
    // Get timestamp from before 24 hours
    const oneDayInMilliseconds = 1000 * 60 * 60 * 24;
    const timeStampBeforeOneDay = Date.now() - oneDayInMilliseconds;

    // Check if "--recent" flag is on
    const showOnlyRecent = str.indexOf('--recent') > -1;

    _data.list('orders', (err, ordersId) => {
        if (!err && ordersId && ordersId.length > 0) {
            ordersId.forEach(orderId => {
                _data.read('orders', orderId, (err, orderData) => {
                    if (!err && orderData) {
                        // If --recent flag exist, show the order if it was created in the past 24 hours
                        const showOrder = showOnlyRecent ? orderData.createdAt > timeStampBeforeOneDay : true;

                        if (showOrder) {
                            const line = 'ID:' + orderData.id + ' - User Email: ' + orderData.userEmail + ' - Amount: $' + orderData.totalAmount + ' Created - ' + new Date(orderData.createdAt);
                            _cliHelpers.verticalSpace();
                            console.log(line);
                        }
                    }
                });
            });
        }
    });
};

// Order info
cli.responders.moreOrderInfo = (str) => {
    // Get the ID from the string
    const arr = str.split('--');
    const orderId = typeof (arr[1]) == 'string' && arr[1].trim().length > 0 ? arr[1].trim() : false;
    if (orderId) {
        // Look the order
        _data.read('orders', orderId, (err, orderData) => {
            if (!err && orderData) {
                // Print the JSON with text highlighting
                _cliHelpers.verticalSpace();
                console.dir(orderData, {
                    'colors': true
                });
                _cliHelpers.verticalSpace();
            }
        });
    }
};

// List users
cli.responders.listUsers = (str) => {

    // Get timestamp from before 24 hours
    const oneDayInMilliseconds = 1000 * 60 * 60 * 24;
    const timeStampBeforeOneDay = Date.now() - oneDayInMilliseconds;

    // Check if "--recent" flag is on
    const showOnlyRecent = str.indexOf('--recent') > -1;

    _data.list('users', (err, userIds) => {
        if (!err && userIds && userIds.length > 0) {
            userIds.forEach(userId => {
                _data.read('users', userId, (err, userData) => {
                    if (!err && userData) {
                        // If --recent flag exist, show the order if it was created in the past 24 hours
                        const showUser = showOnlyRecent ? userData.createdAt > timeStampBeforeOneDay : true;

                        if (showUser) {
                            let line = 'Name: ' + userData.firstName + ' ' + userData.lastName + ' Email: ' + userData.email;
                            const shoppingCartId = !!userData.shoppingCarts.length ? userData.shoppingCarts[0] : false;
                            const shoppingCartString = !!shoppingCartId ? ' Shopping Cart Id:' + shoppingCartId : '';
                            line += shoppingCartString;
                            line += ' Created - ' + new Date(userData.createdAt);
                            _cliHelpers.verticalSpace();
                            console.log(line);
                        }
                    }
                });
            });
        }
    });
};

// More user info
cli.responders.moreUserInfo = (str) => {
    // Get the email from the string
    const arr = str.split('--');
    const userEmail = typeof (arr[1]) == 'string' && arr[1].trim().length > 0 ? arr[1].trim() : false;
    if (userEmail) {
        // Look the user
        _data.read('users', userEmail, (err, userData) => {
            if (!err && userData) {
                // Remove the hashed password
                delete userData.hashedPassowrd;

                // Print the JSON with text highlighting
                _cliHelpers.verticalSpace();
                console.dir(userData, {
                    'colors': true
                });
                _cliHelpers.verticalSpace();
            }
        });
    }
};

/**
 * INPUT PROCESSOR
 */
cli.processInput = (str) => {
    str = typeof (str) == 'string' && str.trim().length > 0 ? str.trim() : false;
    // Only process the input if the user actually wrote something. Otherwise ignore
    if (str) {
        // Codify the unique strings that indentify the unique questions that the user can asks
        const uniqueInputs = [
            'man',
            'help',
            'exit',
            'list menu items',
            'list orders',
            'more order info',
            'list users',
            'more user info'
        ];

        // Go through the possible inputs, emit an event when a match is found
        let matchFound = false;
        uniqueInputs.some((input) => {
            if (str.toLowerCase().indexOf(input) > -1) {
                matchFound = true;
                // Emit an event matching the unique input, and include the full string given
                e.emit(input, str);
                return true;
            }
        });

        // If no match is found, tell the user to try again
        if (!matchFound) {
            console.log('Sorry, try again');
        }
    }
};

// Init script
cli.init = () => {
    // Send the start message to the console, in dark blue
    console.log('\x1b[34m%s\x1b[0m', 'The CLI is running');

    // Start the interface
    const _interface = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        prompt: '>'
    });

    // Create an initial prompt
    _interface.prompt();

    _interface.on('line', (str) => {
        // Send to the input processor
        cli.processInput(str);

        // Re-initialize the prompt afterwards
        _interface.prompt();
    });

    // If the user stops the CLI, kill the associated process
    _interface.on('close', () => {
        process.exit(0);
    });
};


// Export the module
module.exports = cli;