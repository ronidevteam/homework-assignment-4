/**
 * Order Handlers
 */

// Dependencies
const _data = require('../data');
const helpers = require('../helpers');
const tokensHandlers = require('./tokens');

// Define token handlers
const ordersHandlers = {};

// Define acceptable methods
ordersHandlers.acceptableMethods = ['post', 'get'];


// Order - post
// Require data: 
// - email
// - cardNumber
// - cardExpityDate
// - cardHolder
// - cardCVVCode
// - shoppingCartId
// Optional data: none
ordersHandlers.post = (data, callback) => {

    let {
        cardNumber,
        cardExpiryDate,
        cardHolder,
        cardCVVCode,
        shoppingCartId
    } = data.payload;

    shoppingCartId = typeof shoppingCartId == 'string' && shoppingCartId.trim().length == 20 ? shoppingCartId.trim() : false;
    cardNumber = typeof cardNumber == 'string' && cardNumber.trim().length === 16 ? cardNumber.trim() : false;
    cardExpiryDate = typeof cardExpiryDate == 'string' && cardExpiryDate.trim().length > 0 ? cardExpiryDate.trim() : false;
    cardHolder = typeof cardHolder == 'string' && cardHolder.trim().length > 0 ? cardHolder.trim() : false;
    cardCVVCode = typeof cardCVVCode == 'string' && cardCVVCode.trim().length === 3 ? cardCVVCode.trim() : false;

    // Validate required fields
    if (cardNumber && cardExpiryDate && cardHolder && cardCVVCode && shoppingCartId) {
        // Get the token from the headers
        const token = typeof data.headers.token == 'string' ? data.headers.token : false;

        _data.read('shopping-carts', shoppingCartId, (err, shoppingCartData) => {
            if (!err && shoppingCartData) {
                tokensHandlers.verifyToken(token, shoppingCartData.userEmail, (tokenIsValid) => {
                    if (tokenIsValid) {
                        // Create the new order
                        const orderId = helpers.createRandomString(20);
                        let newOrder = {
                            id: orderId,
                            userEmail: shoppingCartData.userEmail,
                            totalAmount: shoppingCartData.totalAmount,
                            shoppingCartId,
                            createdAt: Date.now()
                        };
                        _data.create('orders', orderId, newOrder, (err) => {
                            if (!err) {
                                // Make payment
                                helpers.proceedPayment(shoppingCartData.totalAmount, (err) => {
                                    if (!err) {
                                        helpers.sendEmail(shoppingCartData.userEmail, shoppingCartData.totalAmount, (err) => {
                                            let responseMessage = newOrder;
                                            if (err) {
                                                responseMessage = {
                                                    Message: 'Order successfully placed but could not send confirmation email'
                                                };
                                            }
                                            callback(200, responseMessage);
                                        });
                                    } else {
                                        callback(400, {
                                            Error: "Could not proceed payment"
                                        });
                                    }
                                });
                            } else {
                                callback(500, {
                                    Error: "Could not save the order"
                                });
                            }
                        });


                    } else {
                        callback(403, {
                            Error: 'Missing required token in header, or token is invalid'
                        });
                    }
                });
            } else {
                callback(400, {
                    Error: 'Could not get shopping cart'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

// Orders - get
// Required data: orderId
// Optional data: none
ordersHandlers.get = (data, callback) => {
    const orderId = typeof data.queryStringObject.orderId == 'string' && data.queryStringObject.orderId.trim().length > 0 ? data.queryStringObject.orderId.trim() : false;
    // Get the token from the headers
    const token = typeof data.headers.token == 'string' ? data.headers.token : false;

    // Verify required fields
    if (orderId) {
        _data.read('orders', orderId, (err, orderData) => {
            if (!err && orderData) {
                tokensHandlers.verifyToken(token, orderData.userEmail, (tokenIsValid) => {
                    if (tokenIsValid) {
                        callback(200, orderData);
                    } else {
                        callback(403, {
                            Error: 'Missing required token in header, or token is invalid'
                        });
                    }
                });
            } else {
                callback(400, {
                    Error: 'Error trying to get shopping cart'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

module.exports = ordersHandlers;