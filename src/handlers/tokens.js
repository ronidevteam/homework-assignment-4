/**
 * Tokens Handlers
 */

// Dependencies
const _data = require('../data');
const helpers = require('../helpers');

// Define token handlers
const tokensHandlers = {};

// Define acceptable methods
tokensHandlers.acceptableMethods = ['post', 'get', 'put', 'delete'];

// Tokens - post
// Required data: email, password
// Optional data: none
tokensHandlers.post = (data, callback) => {
    const email =
        typeof data.payload.email == 'string' &&
        data.payload.email.trim().length > 0 &&
        helpers.validateEmail(data.payload.email.trim()) ?
        data.payload.email.trim() :
        false;
    const password =
        typeof data.payload.password == 'string' &&
        data.payload.password.trim().length > 0 ?
        data.payload.password.trim() :
        false;

    if (email && password) {
        // Lookup the user who matches that email
        _data.read('users', email, (err, userData) => {
            if (!err && userData) {
                // Hash the sent password and compare it to the password stored in the user object
                const hashedPassowrd = helpers.hash(password);
                if (hashedPassowrd == userData.hashedPassowrd) {
                    // If valid, create a new token with random name. Set expiration to 1 hour
                    const tokenId = helpers.createRandomString(20);
                    const expires = Date.now() + 1000 * 60 * 60;
                    const tokenObject = {
                        email,
                        id: tokenId,
                        expires
                    };

                    // Store the token
                    _data.create('tokens', tokenId, tokenObject, err => {
                        if (!err) {
                            callback(200, tokenObject);
                        } else {
                            callback(500, {
                                Error: 'Could not create the new token'
                            });
                        }
                    });
                } else {
                    callback(400, {
                        Error: 'Password did not match the specified users stored password'
                    });
                }
            } else {
                callback(400, {
                    Error: 'Could not find the specied user'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required field(s)'
        });
    }
};

// Tokens - get
// Required data : id
// Optional data: none
tokensHandlers.get = (data, callback) => {
    // Check that the id sent is valid
    const id =
        typeof data.queryStringObject.id == 'string' &&
        data.queryStringObject.id.trim().length == 20 ?
        data.queryStringObject.id.trim() :
        false;

    if (id) {
        // Lookup the user
        _data.read('tokens', id, (err, tokenData) => {
            if (!err && tokenData) {
                callback(200, tokenData);
            } else {
                callback(404);
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required field'
        });
    }
};

// Tokens - put
// Required data: id, extend
// Optional data: none
tokensHandlers.put = (data, callback) => {
    const id = typeof data.payload.id == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false;
    const extend = typeof data.payload.extend == 'boolean' && data.payload.extend;

    if (id && extend) {
        _data.read('tokens', id, (err, tokenData) => {
            if (!err && tokenData) {
                // Check to make sure the token isn't already expired
                if (tokenData.expires > Date.now()) {
                    // Set the expiration an hour from now
                    tokenData.expires = Date.now() + 1000 * 60 * 60;

                    // Store the new updates
                    _data.update('tokens', id, tokenData, err => {
                        if (!err) {
                            callback(200);
                        } else {
                            callback(500, {
                                Error: 'Coulnd not update the token expiration'
                            });
                        }
                    });
                } else {
                    callback(400, {
                        Error: 'The token has already expired, and cannot be extended'
                    });
                }
            } else {
                callback(400, {
                    Error: 'Specified token does not exist'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields or fields are invalid'
        });
    }
};

// Tokens - delete
// Required data: id
// Optional data: none
tokensHandlers.delete = (data, callback) => {
    // Check that the id is valid
    const id = typeof data.payload.id == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false;

    if (id) {
        // Lookup the token
        _data.read('tokens', id, (err, data) => {
            if (!err && data) {
                _data.delete('tokens', id, err => {
                    if (!err) {
                        callback(200);
                    } else {
                        callback(500, {
                            Error: 'Could not find the specified token'
                        });
                    }
                });
            } else {
                callback(400, {
                    Error: 'Could not find the specified token'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

// Verify if a given token id is currently valid for a given user
tokensHandlers.verifyToken = (id, email, callback) => {
    // Lookup the token
    _data.read('tokens', id, (err, tokenData) => {
        if (!err && tokenData) {
            // Check that the token is for the given user and has not expired
            if (tokenData.email == email && tokenData.expires > Date.now()) {
                callback(true);
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    });
};




// Export the module
module.exports = tokensHandlers;