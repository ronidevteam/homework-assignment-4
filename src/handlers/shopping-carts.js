/**
 * Shopping Cart Handlers
 */

// Dependencies
const _data = require('../data');
const helpers = require('../helpers');
const tokensHandlers = require('./tokens');
const menu = require('../menu');

// Define token handlers
const shoppingCartsHandlers = {};

// Define acceptable methods
shoppingCartsHandlers.acceptableMethods = ['post', 'get', 'put', 'delete'];

// Shopping cart - post
// Required data:
// - items - array of objects, each consisting of name and quantity(e.g [{name: 'pizza', quantity: 1}])
// - email
// Optional data: none
shoppingCartsHandlers.post = (data, callback) => {
    let {
        items,
        email
    } = data.payload;
    items = typeof items == 'object' && items instanceof Array && items.length > 0 ? items : false;
    email = typeof email == 'string' && email.trim().length > 0 && helpers.validateEmail(email.trim()) ? email.trim() : false;

    // Validate required fields
    if (email && items) {
        // Get the token from the headers
        const token = typeof data.headers.token == 'string' ? data.headers.token : false;

        tokensHandlers.verifyToken(token, email, tokenIsValid => {
            if (tokenIsValid) {
                let invalidProducts = [];
                let validProducts = [];
                let totalAmount = 0;

                // Check if items exist in the menu
                items.forEach(item => {
                    const productFromMenu = menu.getItem(item.name);
                    if (productFromMenu) {
                        item.price = productFromMenu.price;
                        validProducts.push(item);
                        totalAmount += item.quantity * productFromMenu.price;
                    } else {
                        // Save all invalid items(if any)
                        invalidProducts.push(item.name);
                    }
                });

                if (invalidProducts.length === 0) {
                    const shoppingCartId = helpers.createRandomString(20);

                    const shoppingCart = {
                        id: shoppingCartId,
                        userEmail: email,
                        items: items,
                        totalAmount
                    };
                    // Get user's data so we can add the shopping cart id to it
                    _data.read('users', email, (err, userData) => {
                        if (!err && userData) {
                            // Save the shopping cart
                            _data.create('shopping-carts', shoppingCartId, shoppingCart, err => {
                                if (!err) {
                                    // Add shopping cart id to the user
                                    userData.shoppingCarts.push(shoppingCartId);
                                    _data.update('users', email, userData, (err) => {
                                        if (!err) {
                                            callback(200, shoppingCart);
                                        } else {
                                            callback(500, {
                                                Error: 'Could not add shopping cart to the user'
                                            });
                                        }
                                    });
                                } else {
                                    callback(500, {
                                        Error: 'Could not create the shopping cart'
                                    });
                                }
                            });
                        } else {
                            callback(500, {
                                Error: 'Could not find user with this email'
                            });
                        }
                    });

                } else {
                    callback(400, {
                        Error: invalidProducts.join(', ') + ' do not exist in the menu'
                    });
                }
            } else {
                callback(403, {
                    Error: 'Missing required token in header, or token is invalid'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

// Shopping cart - put
// Required data:
// - items - array of objects, each consisting of name and quantity(e.g [{name: 'pizza', quantity: 1}])
// - shoppingCartId
// Optional data: none
shoppingCartsHandlers.put = (data, callback) => {
    let {
        items,
        shoppingCartId
    } = data.payload;
    items = typeof items == 'object' && items instanceof Array ? items : false;
    shoppingCartId = typeof shoppingCartId == 'string' && shoppingCartId.trim().length > 0 ? shoppingCartId.trim() : false;

    // Get the token from the headers
    const token = typeof data.headers.token == 'string' ? data.headers.token : false;

    // Validate required fields
    if (items && shoppingCartId) {
        // Get the shopping cart
        _data.read('shopping-carts', shoppingCartId, (err, shoppingCartData) => {
            if (!err && shoppingCartData) {
                tokensHandlers.verifyToken(token, shoppingCartData.userEmail, tokenIsValid => {
                    if (tokenIsValid) {
                        // Check if items exist in the menu
                        let invalidProducts = [];
                        let validProducts = [];
                        let totalAmount = 0;
                        items.forEach(item => {
                            const productFromMenu = menu.getItem(item.name);
                            if (productFromMenu) {
                                totalAmount += productFromMenu.price * item.quantity;
                                item.price = productFromMenu.price;
                                validProducts.push(item);
                            } else {
                                // Save all invalid items(if any)
                                invalidProducts.push(item.name);
                            }
                        });

                        if (invalidProducts.length === 0) {
                            shoppingCartData.items = items;
                            shoppingCartData.totalAmount = totalAmount;

                            _data.update('shopping-carts', shoppingCartId, shoppingCartData, (err) => {
                                if (!err) {
                                    callback(200, shoppingCartData);
                                } else {
                                    callback(500, {
                                        Error: 'Could not update shopping cart'
                                    });
                                }
                            });
                        } else {
                            callback(400, {
                                Error: invalidProducts.join(', ') + ' do not exist in the menu'
                            });
                        }
                    } else {
                        callback(403, {
                            Error: 'Missing required token in header, or token is invalid'
                        });
                    }
                });
            } else {
                callback(400, {
                    Error: 'Shopping cart with provided ID does not exist for that user'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

// Shopping cart - get
// Required data: shoppingCartId
// Optional data: none
shoppingCartsHandlers.get = (data, callback) => {
    const shoppingCartId = typeof data.queryStringObject.shoppingCartId == 'string' && data.queryStringObject.shoppingCartId.trim().length > 0 ? data.queryStringObject.shoppingCartId.trim() : false;
    // Get the token from the headers
    const token = typeof data.headers.token == 'string' ? data.headers.token : false;
    // Verify required fields
    if (shoppingCartId) {
        _data.read('shopping-carts', shoppingCartId, (err, shoppingCartData) => {
            if (!err && shoppingCartData) {
                tokensHandlers.verifyToken(token, shoppingCartData.userEmail, (tokenIsValid) => {
                    if (tokenIsValid) {
                        callback(200, shoppingCartData);
                    } else {
                        callback(403, {
                            Error: 'Missing required token in header, or token is invalid'
                        });
                    }
                });
            } else {
                callback(400, {
                    Error: 'Error trying to get shopping cart'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

// Shopping cart - delete
// Required data: shoppingCartId
// Optional data: none
shoppingCartsHandlers.delete = (data, callback) => {
    const shoppingCartId = typeof data.queryStringObject.shoppingCartId == 'string' && data.queryStringObject.shoppingCartId.trim().length > 0 ? data.queryStringObject.shoppingCartId.trim() : false;
    // Get the token from the headers
    const token = typeof data.headers.token == 'string' ? data.headers.token : false;

    // Validate required fields
    if (shoppingCartId) {
        _data.read('shopping-carts', shoppingCartId, (err, shoppingCartData) => {
            if (!err && shoppingCartData) {
                tokensHandlers.verifyToken(token, shoppingCartData.userEmail, tokenIsValid => {
                    if (tokenIsValid) {
                        _data.delete('shopping-carts', shoppingCartId, (err) => {
                            if (!err) {
                                callback(200);
                            } else {
                                callback(500, {
                                    Error: 'Could not delete shopping cart'
                                });
                            }
                        });
                    } else {
                        callback(403, {
                            Error: 'Missing required token in header, or token is invalid'
                        });
                    }
                });
            } else {
                callback(400, {
                    Error: 'Could not get shopping cart'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};


module.exports = shoppingCartsHandlers;