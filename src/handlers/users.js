/**
 * Users Handlers
 */

// Dependencies
const _data = require('../data');
const helpers = require('../helpers');
const tokensHandlers = require('./tokens');

const usersHandlers = {};

// Define acceptable methods
usersHandlers.acceptableMethods = ['post', 'get', 'put', 'delete'];

// Users - post
// Required data: firstName, lastName, email, password, street address
// Optional data: none
usersHandlers.post = (data, callback) => {
    // Check that all required fields are filled out
    const firstName =
        typeof data.payload.firstName == 'string' &&
        data.payload.firstName.trim().length > 0 ?
        data.payload.firstName.trim() :
        false;
    const lastName =
        typeof data.payload.lastName == 'string' &&
        data.payload.lastName.trim().length > 0 ?
        data.payload.lastName.trim() :
        false;
    const email =
        typeof data.payload.email == 'string' &&
        data.payload.email.trim().length > 0 &&
        helpers.validateEmail(data.payload.email.trim()) ?
        data.payload.email.trim() :
        false;
    const password =
        typeof data.payload.password == 'string' &&
        data.payload.password.trim().length > 0 ?
        data.payload.password.trim() :
        false;
    const streetAddress =
        typeof data.payload.streetAddress == 'string' &&
        data.payload.streetAddress.trim().length > 0 ?
        data.payload.streetAddress.trim() :
        false;

    if (firstName && lastName && email && password && streetAddress) {
        // Make sude that user does not already exist
        _data.read('users', email, (err, data) => {
            if (err) {
                // Hash the passowrd
                const hashedPassowrd = helpers.hash(password);
                if (hashedPassowrd) {

                    // Create the user object
                    const userObject = {
                        firstName,
                        lastName,
                        email,
                        hashedPassowrd,
                        streetAddress,
                        shoppingCarts: [],
                        createdAt: Date.now()
                    };

                    // Store the user
                    _data.create('users', email, userObject, err => {
                        if (!err) {
                            callback(200);
                        } else {
                            console.log(err);
                            callback(500, {
                                Error: 'Could not create the new user'
                            });
                        }
                    });
                } else {
                    callback(500, {
                        Error: 'Could not hash the users password'
                    });
                }
            } else {
                callback(400, {
                    Error: 'A user with that email address already exist'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fileds'
        });
    }
};

// Users - get
// Require data: email
// Optional data: none
usersHandlers.get = (data, callback) => {
    // Check if the email is valid
    const email =
        typeof data.queryStringObject.email == 'string' &&
        data.queryStringObject.email.trim().length > 0 &&
        helpers.validateEmail(data.queryStringObject.email.trim()) ?
        data.queryStringObject.email.trim() :
        false;

    if (email) {
        // Get the token from the headers
        const token = typeof data.headers.token == 'string' ? data.headers.token : false;
        // Verify that the given token is valid fro the phone number
        tokensHandlers.verifyToken(token, email, tokenIsValid => {
            if (tokenIsValid) {
                // Lookup the user
                _data.read('users', email, (err, data) => {
                    if (!err && data) {
                        // Remove the hashed password from the user object before retuning it to the requester
                        delete data.hashedPassowrd;
                        callback(200, data);
                    } else {
                        callback(404);
                    }
                });
            } else {
                callback(403, {
                    Error: 'Missing required token in header, or token is invalid'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};

// Users - put
// Required data: email
// Optional data: firstName, lastName, password, street address (at least one must be specified)
usersHandlers.put = (data, callback) => {
    // Check for the required field
    const email =
        typeof data.payload.email == 'string' &&
        data.payload.email.trim().length > 0 &&
        helpers.validateEmail(data.payload.email.trim()) ?
        data.payload.email.trim() :
        false;

    // Check for the optional fields;
    const firstName =
        typeof data.payload.firstName == 'string' &&
        data.payload.firstName.trim().length > 0 ?
        data.payload.firstName.trim() :
        false;
    const lastName =
        typeof data.payload.lastName == 'string' &&
        data.payload.lastName.trim().length > 0 ?
        data.payload.lastName.trim() :
        false;
    const password =
        typeof data.payload.password == 'string' &&
        data.payload.password.trim().length > 0 ?
        data.payload.password.trim() :
        false;
    const streetAddress =
        typeof data.payload.streetAddress == 'string' &&
        data.payload.streetAddress.trim().length > 0 ?
        data.payload.streetAddress.trim() :
        false;

    // Error if the email is invalid
    if (email) {
        // Error if nothing is sent to update
        if (firstName || lastName || password || streetAddress) {
            // Get the token from the headers
            const token = typeof data.headers.token == 'string' ? data.headers.token : false;
            tokensHandlers.verifyToken(token, email, tokenIsValid => {
                if (tokenIsValid) {
                    // Lookup the user
                    _data.read('users', email, (err, userData) => {
                        if (!err && userData) {
                            // Update the fields necessary
                            if (firstName) {
                                userData.firstName = firstName;
                            }
                            if (lastName) {
                                userData.lastName = lastName;
                            }
                            if (password) {
                                userData.hashedPassowrd = helpers.hash(password);
                            }
                            if (streetAddress) {
                                userData.streetAddress = streetAddress;
                            }
                            // Store the new updates
                            _data.update('users', email, userData, err => {
                                if (!err) {
                                    callback(200);
                                } else {
                                    callback(500, {
                                        Error: 'Could not update the user'
                                    });
                                }
                            });
                        } else {
                            callback(400, {
                                Error: 'The specified user does not exist'
                            });
                        }
                    });
                } else {
                    callback(403, {
                        Error: 'Missing required token in header, or token is invalid'
                    });
                }
            });
        } else {
            callback(400, {
                Error: 'Missing fields to update'
            });
        }
    } else {
        callback(400, {
            Error: 'Missing required field'
        });
    }
};

// Users - delete
// Required data: email
usersHandlers.delete = (data, callback) => {
    // Check that the email is valid
    const email =
        typeof data.payload.email == 'string' &&
        data.payload.email.trim().length > 0 &&
        helpers.validateEmail(data.payload.email.trim()) ?
        data.payload.email.trim() :
        false;

    if (email) {
        // Get the token from the headers
        const token = typeof data.headers.token == 'string' ? data.headers.token : false;
        tokensHandlers.verifyToken(token, email, tokenIsValid => {
            if (tokenIsValid) {
                // Lookup the user
                _data.read('users', email, (err, userData) => {
                    if (!err && data) {
                        _data.delete('users', email, err => {
                            if (!err) {
                                callback(200);
                            } else {
                                callback(500, {
                                    Error: 'Could not find the specified user'
                                });
                            }
                        });
                    } else {
                        callback(400, {
                            Error: 'Could not find the specified user'
                        });
                    }
                });
            } else {
                callback(403, {
                    Error: 'Missing required token in header, or token is invalid'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};


// Export the module
module.exports = usersHandlers;