/**
 * Request handlers
 */

// Dependencies
const helpers = require('../helpers');
const usersHandlers = require('./users');
const tokensHandlers = require('./tokens');
const menuHandlers = require('./menu');
const shoppingCartsHandlers = require('./shopping-carts');
const ordersHandlers = require('./orders');

// Define handlers
let handlers = {};





/**********************************************************
 * WEBSITE HANDLERS
 **********************************************************/

// Index page
// Index handler
handlers.index = (data, callback) => {
    // Reject any request that isn't a GET
    if (data.method == 'get') {

        // Prepate data for interpolation 
        const tempalteData = {
            'head.title': 'Pizza Palace',
            'head.description': 'Best pizza place in town',
            'body.class': 'index'
        }

        // Read in a template as a string
        helpers.getTemplate('index', tempalteData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, tempalteData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500, undefined, 'html');
            }
        });
    } else {

        callback(405, undefined, 'html');
    }
};


// Menu page
handlers.menuDisplay = (data, callback) => {
    // Allow only GET method
    if (data.method == 'get') {
        const templateData = {
            'head.title': 'Pizza Menu',
            'body.class': 'menu'
        };

        helpers.getTemplate('menuDisplay', templateData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, templateData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500);
            }
        });
    } else {
        callback(405);
    }
};

// Shopping cart page
handlers.shoppingCartPreview = (data, callback) => {
    const templateData = {
        'head.title': 'Shopping cart',
        'body.class': 'shoppingCartPreview'
    };

    // Allow only GET method
    if (data.method == 'get') {
        helpers.getTemplate('shoppingCartPreview', templateData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, templateData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500);
            }
        });
    } else {
        callback(405);
    }
};


// Shopping cart page
handlers.orderCreate = (data, callback) => {
    const templateData = {
        'head.title': 'Make order',
        'body.class': 'orderCreate'
    };

    // Allow only GET method
    if (data.method == 'get') {
        helpers.getTemplate('orderCreate', templateData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, templateData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500);
            }
        });
    } else {
        callback(405);
    }
};


// Shopping cart page
handlers.orderCreated = (data, callback) => {
    const templateData = {
        'head.title': 'Make order',
        'body.class': 'orderCreated'
    };

    // Allow only GET method
    if (data.method == 'get') {
        helpers.getTemplate('orderCreated', templateData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, templateData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500);
            }
        });
    } else {
        callback(405);
    }
};

// Signup page
handlers.accountCreate = (data, callback) => {
    const templateData = {
        'head.title': 'Create an account',
        'body.class': 'accountCreate'
    };

    // Allow only GET method
    if (data.method == 'get') {
        helpers.getTemplate('accountCreate', templateData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, templateData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500);
            }
        });
    } else {
        callback(405);
    }
};

// Signup page
handlers.accountLogin = (data, callback) => {
    const templateData = {
        'head.title': 'Log in to your account',
        'body.class': 'accountLogin'
    };

    // Allow only GET method
    if (data.method == 'get') {
        helpers.getTemplate('accountLogin', templateData, (err, str) => {
            if (!err && str) {
                // Add the universal header and footer
                helpers.addUniversalTemplates(str, templateData, (err, str) => {
                    if (!err && str) {
                        callback(200, str, 'html');
                    } else {
                        callback(500, undefined, 'html');
                    }
                });
            } else {
                callback(500);
            }
        });
    } else {
        callback(405);
    }
};



// Favicon
handlers.favicon = (data, callback) => {
    // Reject in the favicon's data
    if (data.method == 'get') {
        // Read in the favicon's data
        helpers.getStaticAsset('favicon.ico', (err, data) => {
            if (!err && data) {
                // Callback the data
                callback(200, data, 'favicon');
            } else {
                callback(500);
            }
        });
    } else {
        callback(405)
    }
};

// Publick assets
handlers.public = (data, callback) => {
    // Reject in the favicon's data
    if (data.method == 'get') {
        // Get the filename being requested
        const trimmedAssetName = data.trimmedPath.replace('public/', '').trim();
        if (trimmedAssetName.length > 0) {
            // Read in the asset's data
            helpers.getStaticAsset(trimmedAssetName, (err, data) => {
                if (!err && data) {

                    // Determine the content type(default to plain text)
                    let contentType = 'plain';
                    if (trimmedAssetName.indexOf('.css') > -1) {
                        contentType = 'css';
                    }
                    if (trimmedAssetName.indexOf('.png') > -1) {
                        contentType = 'png';
                    }

                    if (trimmedAssetName.indexOf('.jpg') > -1) {
                        contentType = 'jpg';
                    }

                    if (trimmedAssetName.indexOf('.ico') > -1) {
                        contentType = 'favicon';
                    }
                    callback(200, data, contentType);
                } else {
                    callback(404);
                }
            });
        } else {
            callback(404);
        }
    } else {
        callback(405);
    }
};


/**********************************************************
 * API HANDLERS
 **********************************************************/

/**
 * USERS
 */
handlers.users = (data, callback) => {
    if (usersHandlers.acceptableMethods.indexOf(data.method) > -1) {
        usersHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * TOKENS
 */
handlers.tokens = (data, callback) => {
    if (tokensHandlers.acceptableMethods.indexOf(data.method) > -1) {
        tokensHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};


/**
 * MENU
 */
handlers.menu = (data, callback) => {
    if (menuHandlers.acceptableMethods.indexOf(data.method) > -1) {
        menuHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * SHOPPING CART
 */
handlers.shoppingCarts = (data, callback) => {
    if (shoppingCartsHandlers.acceptableMethods.indexOf(data.method) > -1) {
        shoppingCartsHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * ORDER
 */
handlers.orders = (data, callback) => {
    if (ordersHandlers.acceptableMethods.indexOf(data.method) > -1) {
        ordersHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * NOT FOUND
 */
handlers.notFound = (data, callback) => {
    callback(404);
};

// Export the module
module.exports = handlers;