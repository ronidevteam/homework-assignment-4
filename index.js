/*
 * Primary file for API
 *
 */

// Dependencies
const server = require('./src/server');
const cli = require('./src/cli');

// Declare the app
const app = {};

// Init function
app.init = () => {
    // Start the server
    server.init();

    // Start the CLI last
    setTimeout(() => {
        cli.init();
    }, 50);
};

// Execute
app.init();


// Export the app
module.exports = app;