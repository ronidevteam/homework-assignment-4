# Node JS Masterclass - Homework Assignment 4

Building a CLI

## Introduction

The App and the API provide a customer with posibility to sign up, preview a menu, add products to a shopping cart, place an order and pay with a credit card. Payments are handled through Stipe. Once a order is made, the user will receive a confirmation email send through Mailgun.

The CLI provides admins with the posibility to preview:

 - All users in the system and users signup in the last 24 hours
 - Details about specific user
 - All orders in the system and orders created in the last 24 hours
 - Details about a specific order
 
## Getting Started

### Prerequisites

- [Stripe Account](https://stripe.com/docs/testing#cards)
- [Mailgun Account](https://mailgun.com)
- Node JS

### Installing

- Clone the repo
- Open the lib directoty. Inside you will see and `example.config.js`. Rename the file to `config.js` and updated the fileds where it says `YOUR_...` with your mailgun and stripe api keys.
- Navigate to the repo from the terminal and run `node index.js`. This will start a server and a CLI. The server listens for requests on port 3000.
- To preview the app, navigate from the browser to `localhost:3000`

## Usage

### Web App

- Navigate from the browser to `localhost:3000`
- Click and signup button and create an accout
- If signup was successful this will redirect you to the menu page.
- Add products to your shopping cart
- Preview the shopping cart
- Place an order
- If order was successfully placed, you will receive an email with the receip

### CLI
- `exit` - Kill the CLI (and the rest of the application)
- `man` - Show this help page
- `help` - Alias of the "man" command
- `list menu items` - Show a list of all menu items
- `list orders --recent` - Show a list of all the orders placed in the system. If "-recent" flag is specified, show only orders made in the last 24 hours"
- `more order info --{orderId}` - Show details of a specific order
- `list users --recent` - Show a list of all the registered users in the system. If "-recent" flag is specified, show only users registered in the last 24 hours
- `more user info --{userEmail}` - Show details of a specific user

### API

- Create a user
- Create a token
- Get the menu
- Create a shopping cart
- Create an order

## API Endpoints

### User

#### Create user

Endpoint - `/users`  
Method - `POST`

--- Payload Data ---

Required:

- firstName (string)
- lastName (string)
- email (string)
- password (string)
- streetAddress (string)

#### Get user

Endpoint - `/users`  
Method - `GET`

--- Query String Data ---

Required

- email

--- Headers ---

- token

#### Edit user

Endpoint - `/users`  
Method - `PUT`

--- Payload Data ---

Required:

- email (string)

Optional (at least one must be specified)

- firstName
- lastName
- password
- street address

--- Headers ---

- token

#### Delete user

Endpoint - `/users`  
Method - `DELETE`

--- Payload Data ---

Required

- email (string)

--- Headers ---

- token

### Token

#### Create token

Endpoint - `/tokens`  
Method - `POST`

--- Payload Data ---

Required

- email (string)

#### Get token

Endpoint - `/tokens`  
Method - `GET`

--- Payload Data ---

Required

- email (string)

## Menu

### Get the menu

Endpoint - `/menu`  
Method - `GET`

--- Headers ---

- token

## Shopping cart

### Create shopping cart

Endpoint - `/shopping-carts`  
Method - `POST`

--- Payload Data ---

Required:

- email (string)
- items (array of objects, each consisting of name and quantity( e.g [{name: 'pizza', quantity: 1}]))

--- Headers ---

- token

### Edit shopping cart

Endpoint - `/shopping-carts`  
Method - `PUT`

--- Payload Data ---

Required:

- shoppingCartId (string)
- items (array of objects, each consisting of name and quantity( e.g [{name: 'pizza', quantity: 1}]))

--- Headers ---

- token

### Get shopping cart

Endpoint - `/shopping-carts`  
Method - `GET`

--- Query string data ---

Required:

- shoppingCartId (string)

--- Headers ---

- token

### Delete shopping cart

Endpoint - `/shopping-carts`  
Method - `DELETE`

--- Payload Data ---

Required:

- shoppingCartId (string)

--- Headers ---

- token

## Orders

### Create order

Endpoint - `/orders`  
Method - `POST`

--- Payload Data ---

Required:

- email (string)
- cardNumber (string; length - 16 characters)
- cardExpityDate (string)
- cardHolder (string)
- cardCVVCode (string - 3 characters)
- shoppingCartId (string)

--- Headers ---

- token

Note: Once an order is placed, you will receive a confirmation email

### Get order

Endpoint - `/orders`  
Method - `GET`

--- Query string data ---

Required:

- orderId (string)

--- Headers ---

- token
