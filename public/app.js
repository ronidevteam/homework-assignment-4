/*
 * Frontend Logic for application
 *
 */

// Container for frontend application
var app = {};

// Config
app.config = {
    'sessionToken': false
};

// AJAX Client (for RESTful API)
app.client = {}

// Interface for making API calls
app.client.request = function (headers, path, method, queryStringObject, payload, callback) {

    // Set defaults
    headers = typeof (headers) == 'object' && headers !== null ? headers : {};
    path = typeof (path) == 'string' ? path : '/';
    method = typeof (method) == 'string' && ['POST', 'GET', 'PUT', 'DELETE'].indexOf(method.toUpperCase()) > -1 ? method.toUpperCase() : 'GET';
    queryStringObject = typeof (queryStringObject) == 'object' && queryStringObject !== null ? queryStringObject : {};
    payload = typeof (payload) == 'object' && payload !== null ? payload : {};
    callback = typeof (callback) == 'function' ? callback : false;

    // For each query string parameter sent, add it to the path
    var requestUrl = path + '?';
    var counter = 0;
    for (var queryKey in queryStringObject) {
        if (queryStringObject.hasOwnProperty(queryKey)) {
            counter++;
            // If at least one query string parameter has already been added, preprend new ones with an ampersand
            if (counter > 1) {
                requestUrl += '&';
            }
            // Add the key and value
            requestUrl += queryKey + '=' + queryStringObject[queryKey];
        }
    }

    // Form the http request as a JSON type
    var xhr = new XMLHttpRequest();
    xhr.open(method, requestUrl, true);
    xhr.setRequestHeader("Content-type", "application/json");

    // For each header sent, add it to the request
    for (var headerKey in headers) {
        if (headers.hasOwnProperty(headerKey)) {
            xhr.setRequestHeader(headerKey, headers[headerKey]);
        }
    }

    // If there is a current session token set, add that as a header
    if (app.config.sessionToken) {
        xhr.setRequestHeader("token", app.config.sessionToken.id);
    }

    // When the request comes back, handle the response
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            var statusCode = xhr.status;
            var responseReturned = xhr.responseText;

            // Callback if requested
            if (callback) {
                try {
                    var parsedResponse = JSON.parse(responseReturned);
                    callback(statusCode, parsedResponse);
                } catch (e) {
                    callback(statusCode, false);
                }

            }
        }
    }

    // Send the payload as JSON
    var payloadString = JSON.stringify(payload);
    xhr.send(payloadString);

};



/*********************************************************************
 * SHOPPING CART
 *********************************************************************/

app.shoppingCart = {};
app.shoppingCart.data = null;
app.shoppingCart.init = function (shoppingCartId, callback) {
    if (shoppingCartId) {
        var queryStringObject = {
            shoppingCartId: shoppingCartId
        };
        app.client.request(undefined, 'api/shopping-carts', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
            if (statusCode == 200 && responsePayload) {
                app.shoppingCart.data = responsePayload;
                callback();
            } else {
                app.shoppingCart.data = {
                    id: null,
                    items: [],
                    totalAmount: 0,
                    userEmail: app.config.sessionToken.email
                };
                callback();
            }
        });
    } else {
        app.shoppingCart.data = {
            id: null,
            items: [],
            totalAmount: 0,
            userEmail: app.config.sessionToken.email
        };
        callback();
    }
};

app.shoppingCart.create = function (callback) {
    var payload = {
        items: app.shoppingCart.data.items,
        email: app.config.sessionToken.email
    };
    app.client.request(undefined, 'api/shopping-carts', 'POST', undefined, payload, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
            app.shoppingCart.data = responsePayload;
            callback(true);
        } else {
            console.log('Could not create shopping cart', responsePayload);
            callback(false);
        }
    });
};

app.shoppingCart.update = function (callback) {
    var payload = {
        shoppingCartId: app.shoppingCart.data.id,
        items: app.shoppingCart.data.items,
    };
    app.client.request(undefined, 'api/shopping-carts', 'PUT', undefined, payload, function (statusCode, responsePayload) {
        if (statusCode == 200 && responsePayload) {
            callback(true);
        } else {
            console.log('Could not create shopping cart', responsePayload);
            callback(false);
        }
    });
};

app.shoppingCart.addItem = function (itemObject, callback) {
    app.shoppingCart.data.items.push(itemObject);
    app.shoppingCart.updateOnTheServer(callback);
};

app.shoppingCart.removeItem = function (itemName, callback) {
    var itemToRemove = app.shoppingCart.data.items.find(function (item) {
        return item.name == itemName;
    });

    app.shoppingCart.data.items.splice(app.shoppingCart.data.items.indexOf(itemToRemove), 1);
    app.shoppingCart.updateOnTheServer(callback);
};

app.shoppingCart.addOrUpdateItem = function (item, callback) {
    if (app.shoppingCart.data.items.length == 0) {
        // Add the item
        app.shoppingCart.addItem(item, callback);
    } else {
        // Chec if item is already in the shopping cart. If yes then increment the quality
        var itemToUpdate = app.shoppingCart.data.items.find(function (shoppingCartItem) {
            return shoppingCartItem.name == item.name;
        });
        if (itemToUpdate) {
            app.shoppingCart.data.items[app.shoppingCart.data.items.indexOf(itemToUpdate)].quantity += item.quantity;
            app.shoppingCart.updateOnTheServer(callback);
        } else {
            app.shoppingCart.addItem(item, callback);
        }
    }
};

app.shoppingCart.updateOnTheServer = function (callback) {
    if (app.shoppingCart.data.id) {
        app.shoppingCart.update(callback);
    } else {
        app.shoppingCart.create(callback);
    }
};



/*********************************************************************
 * APP LOGIC
 *********************************************************************/

// Bind the logout button
app.bindLogoutButton = function () {
    document.getElementById("logoutButton").addEventListener("click", function (e) {

        // Stop it from redirecting anywhere
        e.preventDefault();

        // Log the user out
        app.logUserOut();

    });
};

// Log the user out then redirect them
app.logUserOut = function (redirectUser) {
    // Set redirectUser to default to true
    redirectUser = typeof (redirectUser) == 'boolean' ? redirectUser : true;

    // Get the current token id
    var tokenId = typeof (app.config.sessionToken.id) == 'string' ? app.config.sessionToken.id : false;

    // Send the current token to the tokens endpoint to delete it
    var queryStringObject = {
        'id': tokenId
    };
    app.client.request(undefined, 'api/tokens', 'DELETE', queryStringObject, undefined, function (statusCode, responsePayload) {
        // Set the app.config token as false
        app.setSessionToken(false);

        // Send the user to the logged out page
        if (redirectUser) {
            window.location = '/';
        }

    });
};

// Bind the forms
app.bindForms = function () {
    if (document.querySelector("form")) {

        var allForms = document.querySelectorAll("form");
        for (var i = 0; i < allForms.length; i++) {
            allForms[i].addEventListener("submit", function (e) {

                // Stop it from submitting
                e.preventDefault();
                var formId = this.id;
                var path = this.action;
                var method = this.method.toUpperCase();

                // Hide the error message (if it's currently shown due to a previous error)
                document.querySelector("#" + formId + " .formError").style.display = 'none';

                // Hide the success message (if it's currently shown due to a previous error)
                if (document.querySelector("#" + formId + " .formSuccess")) {
                    document.querySelector("#" + formId + " .formSuccess").style.display = 'none';
                }


                // Turn the inputs into a payload
                var payload = {};
                var elements = this.elements;
                for (var i = 0; i < elements.length; i++) {
                    if (elements[i].type !== 'submit') {
                        // Determine class of element and set value accordingly
                        var classOfElement = typeof (elements[i].classList.value) == 'string' && elements[i].classList.value.length > 0 ? elements[i].classList.value : '';
                        var valueOfElement = elements[i].type == 'checkbox' && classOfElement.indexOf('multiselect') == -1 ? elements[i].checked : classOfElement.indexOf('intval') == -1 ? elements[i].value : parseInt(elements[i].value);
                        var elementIsChecked = elements[i].checked;
                        // Override the method of the form if the input's name is _method
                        var nameOfElement = elements[i].name;
                        if (nameOfElement == '_method') {
                            method = valueOfElement;
                        } else {
                            // Create an payload field named "method" if the elements name is actually httpmethod
                            if (nameOfElement == 'httpmethod') {
                                nameOfElement = 'method';
                            }
                            // Create an payload field named "id" if the elements name is actually uid
                            if (nameOfElement == 'uid') {
                                nameOfElement = 'id';
                            }
                            // If the element has the class "multiselect" add its value(s) as array elements
                            if (classOfElement.indexOf('multiselect') > -1) {
                                if (elementIsChecked) {
                                    payload[nameOfElement] = typeof (payload[nameOfElement]) == 'object' && payload[nameOfElement] instanceof Array ? payload[nameOfElement] : [];
                                    payload[nameOfElement].push(valueOfElement);
                                }
                            } else {
                                payload[nameOfElement] = valueOfElement;
                            }

                        }
                    }
                }
                // If the method is DELETE, the payload should be a queryStringObject instead
                var queryStringObject = method == 'DELETE' ? payload : {};

                var orderCreateFrom = document.getElementById('orderCreateForm');
                var orderCreateLoadingMessage = document.getElementById('orderCreateLoading');
                if (formId == 'orderCreateForm') {
                    orderCreateFrom.classList.add('hidden');
                    orderCreateLoadingMessage.classList.remove('hidden');
                    payload.shoppingCartId = app.shoppingCart.data.id;
                    payload.email = app.config.sessionToken.email;
                }

                // Call the API
                app.client.request(undefined, path, method, queryStringObject, payload, function (statusCode, responsePayload) {
                    if (formId == 'orderCreateForm') {
                        orderCreateFrom.classList.remove('hidden');
                        orderCreateLoadingMessage.classList.add('hidden');
                    }
                    // Display an error on the form if needed
                    if (statusCode !== 200) {

                        if (statusCode == 403) {
                            // log the user out
                            app.logUserOut();

                        } else {

                            // Try to get the error from the api, or set a default error message
                            var error = typeof (responsePayload.Error) == 'string' ? responsePayload.Error : 'An error has occured, please try again';

                            // Set the formError field with the error text
                            document.querySelector("#" + formId + " .formError").innerHTML = error;

                            // Show (unhide) the form error field on the form
                            document.querySelector("#" + formId + " .formError").style.display = 'block';
                        }
                    } else {
                        // If successful, send to form response processor
                        app.formResponseProcessor(formId, payload, responsePayload);
                    }

                });
            });
        }
    }
};

// Form response processor
app.formResponseProcessor = function (formId, requestPayload, responsePayload) {
    // If account creation was successful, try to immediately log the user in
    if (formId == 'accountCreate') {
        // Take the email and password, and use it to log the user in
        var newPayload = {
            'email': requestPayload.email,
            'password': requestPayload.password
        };

        app.client.request(undefined, 'api/tokens', 'POST', undefined, newPayload, function (newStatusCode, newResponsePayload) {
            // Display an error on the form if needed
            if (newStatusCode !== 200) {

                // Set the formError field with the error text
                document.querySelector("#" + formId + " .formError").innerHTML = 'Sorry, an error has occured. Please try again.';

                // Show (unhide) the form error field on the form
                document.querySelector("#" + formId + " .formError").style.display = 'block';

            } else {
                // If successful, set the token and redirect the user
                app.setSessionToken(newResponsePayload);
                window.location = '/menu';
            }
        });
    }
    if (formId == 'orderCreateForm') {
        window.location = '/order-created';
    }
};

// Get the session token from localstorage and set it in the app.config object
app.getSessionToken = function () {
    var tokenString = localStorage.getItem('token');
    if (typeof (tokenString) == 'string') {
        try {
            var token = JSON.parse(tokenString);
            app.config.sessionToken = token;
            if (typeof (token) == 'object') {
                app.setLoggedInClass(true);
            } else {
                app.setLoggedInClass(false);
            }
        } catch (e) {
            app.config.sessionToken = false;
            app.setLoggedInClass(false);
        }
    }
};

// Set (or remove) the loggedIn class from the body
app.setLoggedInClass = function (add) {
    var target = document.querySelector("body");
    if (add) {
        target.classList.add('loggedIn');
    } else {
        target.classList.remove('loggedIn');
    }
};

// Set the session token in the app.config object as well as localstorage
app.setSessionToken = function (token) {
    app.config.sessionToken = token;
    var tokenString = JSON.stringify(token);
    localStorage.setItem('token', tokenString);
    if (typeof (token) == 'object') {
        app.setLoggedInClass(true);
    } else {
        app.setLoggedInClass(false);
    }
};

// Renew the token
app.renewToken = function (callback) {
    var currentToken = typeof (app.config.sessionToken) == 'object' ? app.config.sessionToken : false;
    if (currentToken) {
        // Update the token with a new expiration
        var payload = {
            'id': currentToken.id,
            'extend': true,
        };
        app.client.request(undefined, 'api/tokens', 'PUT', undefined, payload, function (statusCode, responsePayload) {
            // Display an error on the form if needed
            if (statusCode == 200) {
                // Get the new token details
                var queryStringObject = {
                    'id': currentToken.id
                };
                app.client.request(undefined, 'api/tokens', 'GET', queryStringObject, undefined, function (statusCode, responsePayload) {
                    // Display an error on the form if needed
                    if (statusCode == 200) {
                        app.setSessionToken(responsePayload);
                        callback(false);
                    } else {
                        app.setSessionToken(false);
                        callback(true);
                    }
                });
            } else {
                app.setSessionToken(false);
                callback(true);
            }
        });
    } else {
        app.setSessionToken(false);
        callback(true);
    }
};

// Load data on the page
app.loadDataOnPage = function () {
    // Get the current page from the body class
    var bodyClasses = document.querySelector("body").classList;
    var primaryClass = typeof (bodyClasses[0]) == 'string' ? bodyClasses[0] : false;

    // Logic for menu page
    if (primaryClass == 'menu') {
        app.loadMenuPage();
    }
    if (primaryClass == 'orderCreate') {
        app.loadOrderCreatePage();
    }

    // Logic for dashboard page
    if (primaryClass == 'shoppingCartPreview') {
        app.loadShoppingCartPage();
    }
};

// Load the menu page
app.loadMenuPage = function () {
    // Get the email from the current token, or log the user out if none is there
    var email = typeof (app.config.sessionToken.email) == 'string' ? app.config.sessionToken.email : false;
    if (email) {
        // Fetch the user data
        var queryStringObject = {
            'email': email
        };
        app.client.request(undefined, 'api/users', 'GET', queryStringObject, undefined, function (statusCode, usersResponsePayload) {
            if (statusCode == 200) {
                if (usersResponsePayload) {
                    app.shoppingCart.init(usersResponsePayload.shoppingCarts[0], function () {
                        app.client.request(undefined, 'api/menu', 'GET', queryStringObject, undefined, function (statusCode, menuPayload) {
                            if (statusCode == 200 && menuPayload) {
                                var menuItems = menuPayload;
                                var menuContainer = document.getElementById('menu-list');
                                var pizzaItemTemplate = menuContainer.getElementsByClassName('column')[0];
                                var pizzaItemTemplateString = menuContainer.innerHTML;

                                var finalString = '';
                                menuItems.forEach(function (item) {
                                    var pizzaItemString = pizzaItemTemplateString.replace('{{pizzaImageName}}', item.name.toLowerCase());
                                    pizzaItemString = pizzaItemString.replace('{{pizzaName}}', item.name);
                                    pizzaItemString = pizzaItemString.replace('{{pizzaName}}', item.name);
                                    pizzaItemString = pizzaItemString.replace('{{pizzaPrice}}', '$' + item.price);
                                    finalString += pizzaItemString;
                                });
                                menuContainer.removeChild(pizzaItemTemplate);
                                menuContainer.innerHTML = finalString;
                                var addToCartButtons = document.getElementsByClassName('js-addToCart');
                                for (var i = 0; i < addToCartButtons.length; i++) {
                                    var button = addToCartButtons[i];
                                    button.addEventListener('click', app.getItemFromUIAndAddToShoppingCart);
                                }
                            } else {
                                console.log("Error trying to load menu");
                            }
                        });
                    });
                }
            } else {
                // If the request comes back as something other than 200, log the user our (on the assumption that the api is temporarily down or the users token is bad)
                app.logUserOut();
            }
        });
    } else {
        app.logUserOut();
    }
};

app.loadOrderCreatePage = function () {
    // Get the email from the current token, or log the user out if none is there
    var email = typeof (app.config.sessionToken.email) == 'string' ? app.config.sessionToken.email : false;
    if (email) {

        // Fetch the user data
        var queryStringObject = {
            'email': email
        };
        app.client.request(undefined, 'api/users', 'GET', queryStringObject, undefined, function (statusCode, usersResponsePayload) {
            if (statusCode == 200) {
                if (usersResponsePayload) {
                    app.shoppingCart.init(usersResponsePayload.shoppingCarts[0], function () {});
                }
            } else {
                app.logUserOut();
            }
        });
    } else {
        app.logUserOut();
    }
}


// Load the shopping cart page
app.loadShoppingCartPage = function () {
    // Get the email from the current token, or log the user out if none is there
    var email = typeof (app.config.sessionToken.email) == 'string' ? app.config.sessionToken.email : false;
    if (email) {

        // Fetch the user data
        var queryStringObject = {
            'email': email
        };
        app.client.request(undefined, 'api/users', 'GET', queryStringObject, undefined, function (statusCode, usersResponsePayload) {
            if (statusCode == 200) {
                if (usersResponsePayload) {
                    app.shoppingCart.init(usersResponsePayload.shoppingCarts[0], function () {
                        var tableContainer = document.getElementById('shoppingCartTableContainer');
                        var table = document.getElementById("shoppingCartPreviewTable");
                        var emptyShoppingCartMessage = document.getElementById('shoppingCartEmptyMessage');
                        if (app.shoppingCart.data.items.length) {
                            tableContainer.classList.remove('hidden');
                            emptyShoppingCartMessage.classList.add('hidden');
                            var shoppingCartData = app.shoppingCart.data;

                            var tableBody = table.getElementsByTagName('tbody')[0];
                            var totalAmout = 0;
                            shoppingCartData.items.forEach(function (item) {
                                totalAmout += item.price * item.quantity;
                                var tableRow = tableBody.insertRow();
                                tableRow.setAttribute('data-item-name', item.name);
                                var td0 = tableRow.insertCell(0);
                                var td1 = tableRow.insertCell(1);
                                var td2 = tableRow.insertCell(2);
                                var td3 = tableRow.insertCell(3);
                                var td4 = tableRow.insertCell(4);
                                td0.innerHTML = item.name;
                                td1.innerHTML = item.quantity;
                                td2.innerHTML = '$' + item.price;
                                td3.innerHTML = '$' + (item.quantity * item.price);
                                var removeItemButton = document.createElement('button');
                                removeItemButton.innerText = 'Remove';
                                removeItemButton.classList.add('button');
                                removeItemButton.classList.add('is-danger');
                                removeItemButton.addEventListener('click', function (event) {
                                    app.removeItemFromDomAndShoppingCart(item.name);
                                });
                                td4.appendChild(removeItemButton);
                            });
                            var totalRow = tableBody.insertRow();
                            var td0 = totalRow.insertCell(0);
                            totalRow.insertCell(1);
                            totalRow.insertCell(2);
                            var totalAmoutCell = totalRow.insertCell(3);
                            td0.innerHTML = '<strong>Total</strong>';
                            totalAmoutCell.innerHTML = '<strong>$' + totalAmout + '<strong>';
                        }
                    });
                }
            } else {
                app.logUserOut();
            }
        });
    } else {
        app.logUserOut();
    }
};

app.removeItemFromDomAndShoppingCart = function (itemName) {
    app.shoppingCart.removeItem(itemName, function () {
        window.location = '/shopping-cart';
    });
};

app.getItemFromUIAndAddToShoppingCart = function () {
    var selectButton = event.target;
    var selectedPizzaName = selectButton.dataset.pizza;
    var selectedPizzaQuantity = Number(selectButton.parentElement.getElementsByTagName('input')[0].value);

    var errorMessage = selectButton.parentElement.getElementsByClassName('js-pizza-error')[0];
    var successMessage = selectButton.parentElement.getElementsByClassName('js-pizza-success')[0];

    // Hide both messages
    errorMessage.classList.add('hidden');
    successMessage.classList.add('hidden');
    if (selectedPizzaQuantity > 0 && selectedPizzaQuantity < 100) {
        var pizzaItem = {
            name: selectedPizzaName,
            quantity: selectedPizzaQuantity
        };

        app.shoppingCart.addOrUpdateItem(pizzaItem, function () {
            successMessage.classList.remove('hidden');
        });
    } else {
        errorMessage.classList.remove('hidden');
    }
};


// Loop to renew token often
app.tokenRenewalLoop = function () {
    setInterval(function () {
        app.renewToken(function (err) {
            if (!err) {
                console.log("Token renewed successfully @ " + Date.now());
            }
        });
    }, 1000 * 60);
};

// Init (bootstrapping)
app.init = function () {

    // Bind all form submissions
    app.bindForms();

    // Bind logout logout button
    app.bindLogoutButton();

    // Get the token from localstorage
    app.getSessionToken();

    // Renew token
    app.tokenRenewalLoop();

    // Load data on page
    app.loadDataOnPage();

};

// Call the init processes after the window loads
window.onload = function () {
    app.init();
};